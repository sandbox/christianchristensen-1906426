<?php

/**
 * @file
 * UI Page callbacks for the xbox module.
 */

/**
 * Menu callback; Shows the Xbox Games, their votes and availability.
 */
function xbox_games_page() {
  $service = wsclient_service_load('xbox');
  $results = array_key_exists('getGames', $service->actions()) ? $service->getGames(variable_get('xbox_apikey', '')) : array();

  $header = array('Title', 'Votes');
  $rows = array();
  $want = array_filter($results, function($x) { return $x->status == 'wantit'; });
  usort($want, function($a, $b) { return strcmp($b->votes, $a->votes); });
  foreach ($want as $result) {
    $votes = $result->votes;
    if (_xbox_vote_add_valid()) {
      $votes .= ' (' . l('+1', 'game/' . $result->id . '/vote', array(
        'attributes' => array(
          'title' => t('Vote ' . $result->title . ' up one point.'),
         )
      )) . ')';
    }
    $rows[] = array($result->title, $votes);
  }
  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'caption' => t('Want'),
    'empty' => t('No titles.'),
  ));
  if (_xbox_vote_add_valid()) {
    $output .= l(t('Add game'), 'addgame');
  }

  $header = array('Title');
  $rows = array();
  $own = array_filter($results, function($x) { return $x->status == 'gotit'; });
  usort($own, function($a, $b) { return strcmp($a->title, $b->title); });
  foreach ($own as $result) {
    $rows[] = array($result->title);
  }
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'caption' => t('Own'),
    'empty' => t('No titles.'),
  ));
  $output .= l(t('Mark purchased game'), 'games/mark');

  return $output;
}

/**
 * Menu form callback; Mark given game(s) as "got" or purchased.
 */
function xbox_mark_games_form($form, &$form_state) {
  $service = wsclient_service_load('xbox');
  $results = array_key_exists('getGames', $service->actions()) ? $service->getGames(variable_get('xbox_apikey', '')) : array();

  $header = array('Title', 'Votes');
  $options = array();
  $want = array_filter($results, function($x) { return $x->status == 'wantit'; });
  usort($want, function($a, $b) { return strcmp($b->votes, $a->votes); });
  foreach ($want as $result) {
    $options[$result->id] = array($result->title, $result->votes);
  }

  $form['games'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No games found'),
    '#multiple' => FALSE,
   );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Form submission handler for xbox_mark_games_form().
 */
function xbox_mark_games_form_submit($form, &$form_state) {
  $game_id = $form_state['values']['games'];

  if (!is_array($game_id)) {
    $service = wsclient_service_load('xbox');
    $result = array_key_exists('setGotIt', $service->actions()) ? $service->setGotIt(variable_get('xbox_apikey', ''), $game_id) : array();
    if ($result) {
      $title = $form['games']['#options'][$game_id][0];
      drupal_set_message(t($title . ' marked as owned.'));
    }
  }
}

/**
 * Menu callback; +1 votes a given xbox game id.
 */
function xbox_vote_game_page($game_id) {
  global $base_path;

  if (is_numeric($game_id)) {
    $service = wsclient_service_load('xbox');
    $result = array_key_exists('addVote', $service->actions()) ? $service->addVote(variable_get('xbox_apikey', ''), $game_id) : array();
    if ($result) {
      drupal_session_start();
      setcookie('Drupal.xbox.activity', time(), time()+60*60*24, $base_path);
      drupal_set_message(t('Vote registered.'));
    }
    else {
      drupal_set_message(t('An error occured while voting.'));
    }
  }
  drupal_goto('<front>');
}

/**
 * Menu form callback; Add an Xbox game.
 */
function xbox_add_game_form($form, &$form_state) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Form validation handler for xbox_add_game_form().
 */
function xbox_add_game_form_validate($form, &$form_state) {
  $title = trim($form_state['values']['title']);

  $service = wsclient_service_load('xbox');
  $results = array_key_exists('getGames', $service->actions()) ? $service->getGames(variable_get('xbox_apikey', '')) : array();

  $existing_titles = array_filter($results, function($x) use($title) { return strcmp(strtolower($x->title), strtolower($title)) === 0;});
  if (count($existing_titles)) {
    form_set_error('title', t('There is a already an entry with that title.'));
  }
}

/**
 * Form submission handler for xbox_add_game_form().
 */
function xbox_add_game_form_submit($form, &$form_state) {
  global $base_path;

  $title = trim($form_state['values']['title']);
  $service = wsclient_service_load('xbox');
  $results = array_key_exists('addGame', $service->actions()) ?
   $service->addGame(variable_get('xbox_apikey', ''), $title) : array();
  drupal_session_start();
  setcookie('Drupal.xbox.activity', time(), time()+60*60*24, $base_path);

  // Jump to the front page
  drupal_set_message('Added ' . $title);
  $form_state['redirect'] = '';
}
