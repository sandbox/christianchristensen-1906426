Xbox Game Voting module (xbox)
------------------------------
UI and business rules to interface with the xbox SOAP service.

Config
------

Variables

 * xbox_wsdl - the SOAP WSDL definition to load.
 * xbox_apikey - the API key to use for the v2 WSDL.

