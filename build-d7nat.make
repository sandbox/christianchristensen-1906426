api = 2
core = 7.x

includes[] = drupal-org-core.make
includes[] = drupal-org.make

projects[d7nat][version] = 7.x-1.x
projects[d7nat][type] = profile
projects[d7nat][download][type] = git
projects[d7nat][download][url] = http://git.drupal.org/sandbox/christianchristensen/1906426.git
projects[d7nat][download][branch] = 7.x-1.x
