# Xbox Game Voting Drupal Application

## Web service and UI interaction

A web service exists to store data with the game and vote information. A user interface will be developed to display the games we own, display games we are voting on, and allow users to submit new titles for voting, and designate a game as owned.


### UI

*  Display Games (tabular?)
   *  (Live?) From `getGames`
   *  Want, by vote count desc.
   *  Own, by name alpha.
*  Vote for a game (only on Want) `addVote`
*  Add New Title/Game `addGame`
   *  No duplicate titles (case?)
*  Voting and Add New Title Restrictions
   *  One vote *or* add per day (midnight to midnight - timezone?)
   *  No activity on Saturday or Sunday
*  Marking a Game as Owned `setGotIt`
   *  Separate page
   *  Cron task - weekly
*  `checkKey`
*  `clearGames`


## Questions / Concerns

1. Setting a cookie to determine whether or not the user has voted is an acceptable low tech solution
   *  is this required or can it be more rigorous (e.g. using the user's account)?
2. Each employee at The Nerdery should be able to vote for their favorite game or add a new game to this list once per day.
   *  Is each employee (machine?) tracked by ^ a cookie or an account (e.g. are there accounts managed on this system or is it simply a front end with session managed by cookie state - no accounts per user?)
3. At the end of the week, if we reach our productivity goals the game with the most votes will be purchased and marked as a game we currently own.
   *  How do we know if productivity goals have been reached?
   *  How is purchasing handled? (out of scope?)
4. Is the SOAP service data only managed by this app or can other apps interact it?
   *  Should the app even manage a local database? (from the requirements listed here this app seems to only require client/session state; no internal application state)
5. Hosting / DNS?
6. Timezone?
   *  Midnight to midnight
   *  Saturday and Sunday
7. Are these requirements expected to change? (e.g. how flexible should the business rules be?)


### Scalability / capacity thoughts

*  Live updating from SOAP service. {Cache, batch, queue,...}
*  Dataset size
*  Audience size of voting vs. viewing; etc...


## Errata

*  p.2 line 1: "...those that we currently want, and those [that] we own."


## Notes

*  Current impl. uses anon. function callbacks => PHP 5.3+
*  [Distro packaging](http://drupal.org/developing/distributions/drupalorg)
*  `drush make build-d7nat.make www` Makes the drupal site+deps to the `www` directory
*  `drush si d7nat --site-name='Xbox Game Voting App' --db-url=sqlite://sites/default/files/.ht.sqlite` Installs the site with a sqlite DB
