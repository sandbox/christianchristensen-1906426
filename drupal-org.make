api = 2
core = 7.x

; Contributed modules.

projects[entity][version] = 1.0
projects[entity][subdir] = contrib

projects[wsclient][version] = 1.0-beta2
projects[wsclient][subdir] = contrib
